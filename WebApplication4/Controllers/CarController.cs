﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApplication4.Models;
using WebApplication4.Services.Interfaces;

namespace WebApplication4.Controllers
{
    public class CarController : Controller
    {
        private readonly IRepository _repository;

        public CarController(IRepository repository)
        {
            _repository = repository;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var model = _repository.GetAll();
            return View(model);
        }

        [HttpGet]
        public IActionResult Create()
        {
            ViewBag.CarsCount = _repository.GetAll().Count;
            return View();
        }

        [HttpPost]
        public IActionResult Create(Car car)
        {
            if (ModelState.IsValid)
            {
                _repository.Create(car);
            }

            ViewBag.CarsCount = _repository.GetAll().Count;
            return View();
        }
    }
}
