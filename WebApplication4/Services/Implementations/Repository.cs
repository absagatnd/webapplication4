﻿using System.Collections.Generic;
using System.Linq;
using WebApplication4.Models;
using WebApplication4.Services.Interfaces;

namespace WebApplication4.Services.Implementations
{
    public class Repository : IRepository
    {
        private readonly IList<Car> _cars; // использовать DbContext вместо списка

        public Repository()
        {
            _cars = new List<Car>()
            {
                new Car()
                {
                    Id = 1,
                    Kuzov = Kuzov.Sedan,
                    Title = "Mercedes Benz S600"
                },
                new Car()
                {
                    Id = 2,
                    Kuzov = Kuzov.Limousine,
                    Title = "Limousine Chrysler"
                },
            };
        }

        public void Create(Car car)
        {
            _cars.Add(car);
        }

        public void Edit(Car car)
        {
            var car1 = _cars.FirstOrDefault(x => x.Id == car.Id);
            car1.Title = car.Title;
            car1.Kuzov = car.Kuzov;
        }

        public void Delete(Car car)
        {
            _cars.Remove(car);
        }

        public Car Get(int id)
        {
            return _cars.First(x => x.Id == id);
        }

        public IList<Car> GetAll()
        {
            return _cars;
        }
    }
}