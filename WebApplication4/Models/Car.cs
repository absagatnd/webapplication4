﻿using System.ComponentModel.DataAnnotations;

namespace WebApplication4.Models
{
    public class Car
    {
        public int Id { get; set; }

        [Display(Name = "Брэнд")]
        public string Title { get; set; }

        [Display(Name = "Тип кузова")]
        public Kuzov Kuzov { get; set; }

    }
}